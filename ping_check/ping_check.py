import os
import re
import time
import platform
import subprocess
from pathlib import Path
from influxdb import InfluxDBClient

packetloss_regex = r'([0-9\\.]+)%\spacket\sloss'
roundtrip_regex = r'([0-9\\.]+)/([0-9\\.]+)/([0-9\\.]+)/([0-9\\.]+) ms'
fail_name_resolution_regex = r'failure in name resolution'

def ping(target_host,count=5):

    # Option for the number of packets as a function of
    param = '-n' if platform.system().lower()=='windows' else '-c'
    cmd = 'ping {} {} {}'.format(param,str(count),target_host)

    response = subprocess.Popen(cmd, shell=True, stderr=subprocess.STDOUT, stdout=subprocess.PIPE).stdout.read().decode('utf-8')

    try:
        packet_loss_percentage = re.findall(packetloss_regex, response)
        if(len(packet_loss_percentage) > 0):
            packet_loss_percentage = packet_loss_percentage[0]
            packet_loss_percentage = packet_loss_percentage.replace(',', '.')
            packet_loss = float(count * (float(packet_loss_percentage)/100))
            if packet_loss > 0.0:
                print("packet_loss is {}, while percentage is {}".format(packet_loss,packet_loss_percentage))
        else:
            packet_loss = None

        roundtrip_metrics = re.findall(roundtrip_regex, response)
        if(len(roundtrip_metrics) > 0):
            roundtrip_metrics = roundtrip_metrics[0]
            roundtrip_avg = roundtrip_metrics[1]
            roundtrip_max = roundtrip_metrics[2]
            roundtrip_avg = float(roundtrip_avg.replace(',', '.'))
            roundtrip_max = float(roundtrip_max.replace(',', '.'))
        else:
            roundtrip_avg = None
            roundtrip_max = None

        failed_name_resolution = len(re.findall(fail_name_resolution_regex, response))
        
        return (packet_loss,roundtrip_avg,roundtrip_max,failed_name_resolution)
    except:
        print("Can't extract infos from: \n {}".format(response))
        return (None,None,None,None)


def extract_json(packet_loss,roundtrip_avg,roundtrip_max,failed_name_resolution,INFLUX_DB):

    return [
        {
            "measurement" : INFLUX_DB,
            "tags" : {
                "target_host": target_host
            },
            "fields" : {
                "packet_loss": packet_loss,
                "roundtrip_avg": roundtrip_avg,
                "roundtrip_max": roundtrip_max,
                "failed_name_resolution": failed_name_resolution
            }
        }
    ]

def send(data,INFLUX_DB):
    INFLUX_HOST = os.getenv("INFLUXDB_HOST","localhost")
    INFLUX_PORT = int(os.getenv("INFLUXDB_PORT",8086))
    INFLUX_USER = os.getenv("INFLUXDB_USER","user")
    INFLUX_USER_PASSWORD = os.getenv("INFLUXDB_USER_PASSWORD","user")

    client = InfluxDBClient(host=INFLUX_HOST,
                            port=INFLUX_PORT,
                            database=INFLUX_DB,
                            username=INFLUX_USER,
                            password=INFLUX_USER_PASSWORD)

    try:
        client.write_points(data)
    except:
        print("Couldn't write data to Sir Influx.")

target_host = os.getenv("TARGET_HOST","google.de")
ping_count = os.getenv("PING_COUNT","5")
INFLUX_DB = os.getenv("INFLUXDB_DB","network")
while True:
    (packet_loss,roundtrip_avg,roundtrip_max,failed_name_resolution) = ping(target_host,int(ping_count))
    if packet_loss is not None:
        data = extract_json(packet_loss,roundtrip_avg,roundtrip_max,failed_name_resolution,INFLUX_DB)
        send(data,INFLUX_DB)
        time.sleep(1)